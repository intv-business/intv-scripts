#!/bin/bash

cp -r /usr/share/zoneinfo/UTC /etc/localtime

apt-get update 
apt-get install -y redis-server
sed -i '/^bind/d' /etc/redis/redis.conf 
service redis-server restart
