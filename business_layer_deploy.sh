#!/bin/bash

echo -n "Please enter the domain name for the project (eg. intv.demo.com): "
read DOMAIN

echo -n "Please enter the database server host/IP: "
read DB_HOST

echo -n "Please enter the intv DB password: "
read -s DB_PASS

cp -r /usr/share/zoneinfo/UTC /etc/localtime

add-apt-repository -y ppa:ondrej/php5-5.6
apt-get update; apt-get -y upgrade
apt-get install -y nginx-extras php5-cli php5-common php5-mysql php5-gd php5-fpm php5-cgi php5-fpm php-pear php5-mcrypt git mc curl wget libssh2-1-dev libssh2-php php5-pgsql encfs apache2-utils

umount /opt/bl-deploy
rm -rf /opt/bl*

mkdir -p /opt/{bl-enc,bl-deploy}

cd /opt/bl-enc; git clone https://gitlab.com/bukowskee/business-deployment.git .

#check if encfs is mounted
MNT="`mount | grep /opt/bl-deploy | awk '{print $1}'`"
if [ -z "$MNT" ]; then
    echo "/opt/bl-enc is not mounted"
    encfs /opt/bl-enc /opt/bl-deploy
    if [ ! $? -eq 0 ]; then
        echo "Fatal error... exit"
        exit 1
    fi
fi

#cd /opt/bl-deploy/scripts; sh ./pg_install

adduser -q --disabled-password --gecos "" --home /srv/intv intv
mkdir -p /srv/intv/www

cp -a /opt/bl-deploy/intv/* /srv/intv/www/
chown -R intv.intv /srv/intv

su - intv -c "cd /srv/intv/; curl -sS https://getcomposer.org/installer | php"
su - intv -c "cd /srv/intv/; php composer.phar global require 'fxp/composer-asset-plugin:~1.0.3'"
su - intv -c "cd /srv/intv/; php composer.phar config -g github-oauth.github.com 87584cf4279ef7674fa7ef509ce9ebe51d018bed"

cp /srv/intv/composer.phar /usr/bin/composer; chmod 755 /usr/bin/composer; chown root.root /usr/bin/composer

su - intv -c "cd /srv/intv/www/; cp config/db.php.sample config/db.php"
su - intv -c "/bin/bash /srv/intv/www/db_config.sh ${DB_HOST} ${DB_PASS} /srv/intv/www/config/db.php.sample /srv/intv/www/config/db.php"
#su - intv -c "sed -e 's/host=127\.0\.0\.1/host='${DB_HOST}'/' -e 's/'"'"'password'"'"' => '"'"'intv'"'"'/'"'"'password'"'"' => '"'"${DB_PASS}"'"'/' > config/db.php < config/db.php.sample"
su - intv -c "cd /srv/intv/www/; cp config/params.php.sample config/params.php"
su - intv -c "composer config -g github-oauth.github.com 67111fc03bb897fb9cb7ed386ac710b4d658bd34"
su - intv -c "cd /srv/intv/www/; composer install"

su - intv -c "cd /srv/intv/www; /srv/intv/www/yii rbac/init"
su - intv -c "cd /srv/intv/www; /srv/intv/www/yii migrate/up"

su - intv -c "cd /srv/intv/www/; composer install"

# aerospike
find /srv/intv/www/vendor/aerospike/aerospike-client-php/ -name "*.sh" -exec chmod +x {} \;
cd /srv/intv/www/vendor/aerospike/aerospike-client-php/ && sudo php /srv/intv/www/composer.phar run-script post-install-cmd
cd src/aerospike && sudo make install
sudo su
 touch /etc/php5/mods-available/aerospike.ini
 echo "extension=aerospike.so
aerospike.udf.lua_system_path=/usr/local/aerospike/lua
aerospike.udf.lua_user_path=/usr/local/aerospike/usr-lua" > /etc/php5/mods-available/aerospike.ini
exit
cd /etc/php5/cli/conf.d
sudo ln -s ../../mods-available/aerospike.ini 20-aerospike.ini

chown -R intv.intv /srv/intv

cp /opt/bl-deploy/config/php/intv.conf /etc/php5/fpm/pool.d/
sed -i.bak 's/memory_limit = 128M/memory_limit = -1/' /etc/php5/fpm/php.ini

service php5-fpm restart

#cp /opt/bl-deploy/config/

rm /etc/nginx/sites-available/default
rm /etc/nginx/sites-enabled/default

cp /opt/bl-deploy/config/nginx/domain.conf /etc/nginx/sites-available/${DOMAIN}.conf
ln -s /etc/nginx/sites-available/${DOMAIN}.conf /etc/nginx/sites-enabled/${DOMAIN}.conf

sed -i 's/DOMAIN\.NAME/'${DOMAIN}'/' /etc/nginx/sites-available/${DOMAIN}.conf

usermod -a -G intv www-data

mkdir /etc/nginx/secure
cp /opt/bl-deploy/config/nginx/htpasswd /etc/nginx/secure/
chmod 600 /etc/nginx/secure/htpasswd; chown www-data.www-data /etc/nginx/secure/htpasswd

/etc/init.d/nginx restart

#update crontab
cat /srv/intv/www/config/crontab | sudo crontab -u intv -