#!/bin/bash
if [ "$(id -u)" != "0" ]; then
   echo "This script must be run as root" 1>&2
   exit 1
fi

set -e

PACKAGE_NAME="adserver-celery"

OPTS=`getopt -o hv: -l help,version: -- "$@"`
if [ $? != 0 ]
then
    exit 1
fi
eval set -- "$OPTS"
while true ; do
    case "$1" in
        -v|--version)  PACKAGE_VERSION=$2; shift 2;;
        -h|--help) echo "Usage: $0"'''
-v, --version     Define package version
-h, --help        This help'''; exit;;
        --) shift; break;;
    esac
done

if ! [ "$PACKAGE_VERSION" ]; then
  printf "\nPackage parametres are not set. See ./`basename $0` -h\n\n"
  exec ./$0 -h
  exit 1
fi


printf "\nCelery deploy starting...\n"
sleep 1
apt-get update
service supervisor stop

apt-get install $PACKAGE_NAME"="$PACKAGE_VERSION

rm /delivery/config.py
ln -s ~/config/config.py /delivery/config.py

pip3 install -r /delivery/config/requirements.txt

sudo service supervisor start

