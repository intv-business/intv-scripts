#!/bin/bash

REPO=https://gitlab.com/intv-business/aero-conf.git
ENC=/opt/aero-enc
CONF=/opt/aero-conf

cp -r /usr/share/zoneinfo/UTC /etc/localtime

apt-get update
apt-get -y upgrade
apt-get install -y --no-install-recommends \
nginx-extras git python3-pip python3-dev \
wget lsb-release unzip ca-certificates curl encfs mc 

umount ${CONF}
rm -rf $CONF
rm -rf $ENC

mkdir -p $CONF
mkdir -p $ENC

cd $ENC; git clone ${REPO} .

#check if encfs is mounted
MNT="`mount | grep ${CONF} | awk '{print $1}'`"
if [ -z "$MNT" ]; then
    echo "${ENC} is not mounted"
    encfs ${ENC} ${CONF}
    if [ ! $? -eq 0 ]; then
        echo "Fatal error... exit"
        exit 1
    fi
fi

wget -O aerospike.tgz 'http://aerospike.com/download/server/latest/artifact/ubuntu12'
tar -xvf aerospike.tgz

cd aerospike-server-community-*-ubuntu12*; ./asinstall

cp -a ${CONF}/etc/aerospike/aerospike.conf /etc/aerospike/
service aerospike restart

aql -c "CREATE INDEX imps_idx ON adserver-dev.imps (t) NUMERIC"
aql -c "CREATE INDEX clicks_idx ON adserver-dev.clicks (t) NUMERIC"
aql -c "CREATE INDEX convs_idx ON adserver-dev.convs (t) NUMERIC"

