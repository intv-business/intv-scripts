#!/bin/bash
if [ "$(id -u)" != "0" ]; then
   echo "This script must be run as root" 1>&2
   exit 1
fi

set -e

PACKAGE_NAME="adserver"


OPTS=`getopt -o hv: -l help,version: -- "$@"`
if [ $? != 0 ]
then
    exit 1
fi
eval set -- "$OPTS"
while true ; do
    case "$1" in
        -v|--version)  PACKAGE_VERSION=$2; shift 2;;
        -h|--help) echo "Usage: $0"'''
-v, --version     Define package version
-h, --help        This help'''; exit;;
        --) shift; break;;
    esac
done

if ! [ "$PACKAGE_VERSION" ]; then
  printf "\nPackage parametres are not set. See ./`basename $0` -h\n\n"
  exec ./$0 -h
  exit 1
fi


printf "\nAdserver daemon deploy starting...\n"
sleep 1
sudo apt-get update
apt-get install $PACKAGE_NAME"="$PACKAGE_VERSION

sudo rm /etc/adserver/adserver.json
sudo ln -s ~/config/adserver.json /etc/adserver/adserver.json
sudo service adserver restart
