#!/bin/bash

REPO=https://gitlab.com/intv-business/adserver-conf.git
ENC=/opt/ad-enc
CONF=/opt/ad-conf

AD_ENC=/opt/ad-bin-enc
AD_BIN=/opt/ad-bin

ADSRV=/opt/adserver

BSRV=intv-build.moinstall.com
BPATH=adserver_packages
BUSER=repo
BKEY=$CONF/etc/ssh/id_rsa

#/usr/bin/curl -L https://toolbelt.treasuredata.com/sh/install-ubuntu-trusty-td-agent2.sh | sh

cp -r /usr/share/zoneinfo/UTC /etc/localtime

apt-get update; apt-get -y upgrade
apt-get install -y --no-install-recommends \
nginx-extras git python3-pip \
wget lsb-release unzip ca-certificates curl encfs mc \
python3-dev

#/usr/sbin/td-agent-gem install fluent-plugin-forest

#rm -rf /var/lib/apt/lists/*

export GOLANG_VERSION=1.5.1
export GOLANG_DOWNLOAD_URL=http://golang.org/dl/go$GOLANG_VERSION.linux-amd64.tar.gz
export GOLANG_DOWNLOAD_SHA1=46eecd290d8803887dec718c691cc243f2175fe0

/usr/bin/curl -kfsSL "$GOLANG_DOWNLOAD_URL" -o golang.tar.gz \
        && echo "$GOLANG_DOWNLOAD_SHA1  golang.tar.gz" | sha1sum -c - \
        && tar -C /usr/local -xzf golang.tar.gz \
        && rm golang.tar.gz

umount ${CONF}
umount ${AD_BIN}

rm -rf $CONF
rm -rf $ENC
rm -rf $AD_BIN
rm -rf $AD_ENC
rm -rf $ADSRV

mkdir -p $CONF
mkdir -p $ENC
mkdir -p $AD_BIN
mkdir -p $AD_ENC
mkdir -p $ADSRV

cat << EOF > /etc/profile.d/go-vars.sh
#!/bin/bash
export GOPATH=/opt/adserver
export PATH="$GOPATH/bin:$PATH:/usr/local/go/bin"
EOF

export GOPATH=/opt/adserver
export PATH="$GOPATH/bin:$PATH:/usr/local/go/bin"

. /etc/profile.d/go-vars.sh

cd $ENC; git clone ${REPO} .

#check if encfs is mounted
MNT="`mount | grep $CONF | awk '{print $1}'`"
if [ -z "$MNT" ]; then
    echo "$ENC is not mounted"
    encfs $ENC $CONF
    if [ ! $? -eq 0 ]; then
        echo "Fatal error... exit"
        exit 1
    fi
fi

chmod 600 $BKEY
scp -r -i $BKEY ${BUSER}@${BSRV}:${BPATH}/*current.tgz ${AD_ENC}/
cd ${AD_ENC}; tar -zxf adserver-binary-current.tgz; 

MNT="`mount | grep $AD_BIN | awk '{print $1}'`"
if [ -z "$MNT" ]; then
    echo "${AD_ENC}/adserver-bin is not mounted"
    encfs ${AD_ENC}/adserver-bin $AD_BIN
    if [ ! $? -eq 0 ]; then
        echo "Fatal error... exit"
        exit 1
    fi
fi


cp -a $AD_BIN/adserver/pkg $GOPATH/
cp -a $AD_BIN/adserver/bin $GOPATH/

mkdir -p /var/log/drip-adserver; chmod 777 /var/log/drip-adserver

#/etc/init.d/nginx stop
#cp -a /etc/nginx/nginx.conf /etc/nginx/nginx.conf.bak

#update-rc.d -f nginx disable
#update-rc.d -f redis-server disable

#/etc/init.d/supervisor stop
#/etc/init.d/td-agent stop

mkdir -p /etc/drip-adserver
cp -a $CONF/etc/drip-adserver /etc/
cp -a $CONF/etc/init/adserver.conf /etc/init/
#cp -a $CONF/etc/td-agent/td-agent.conf /etc/td-agent/
#cp -a $CONF/etc/td-agent/fluentd /etc/td-agent/plugin/

echo -n "Please enter the MQ (celery/redis) host/IP: "
read CHOST

sed -i 's/delivery_nginx/'$CHOST'/' /etc/drip-adserver/adserver.json

echo -n "Please enter the Aerospike host/IP: "
read AHOST

sed -i 's/"host": "aerospike"/"host": "'$AHOST'"/' /etc/drip-adserver/adserver.json

#ksv
echo -n "Please enter the Redis host/IP: "
read RHOST

if [ $RHOST == "127.0.0.1" ]
then
apt-get install -y redis-server
fi

sed -i 's/tcp\:\/\/redis\:6379/tcp\:\/\/'$RHOST'\:6379/' /etc/drip-adserver/adserver.json

echo -n "Please enter the adserver global domain address: "
read ADSRVHOST

sed -i 's/"domain": "localhost:8081"/"domain": "'$ADSRVHOST'"/' /etc/drip-adserver/adserver.json
#sed -i 's/"host": "fluentd"/"host": "localhost"/' /etc/drip-adserver/adserver.json

#sed -i 's/host fluentd_aggregator/host '$CHOST'/' /etc/td-agent/td-agent.conf


#/etc/init.d/td-agent start
service adserver start




#umount /opt/adserver-deploy
