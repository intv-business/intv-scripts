#!/bin/bash
if [ "$(id -u)" != "0" ]; then
   echo "This script must be run as root" 1>&2
   exit 1
fi

set -e

PACKAGE_NAME="business-layer"

OPTS=`getopt -o hv: -l help,version: -- "$@"`
if [ $? != 0 ]
then
    exit 1
fi
eval set -- "$OPTS"
while true ; do
    case "$1" in
        -v|--version)  PACKAGE_VERSION=$2; shift 2;;
        -h|--help) echo "Usage: $0"'''
-v, --version     define version
-h, --help        this help'''; exit;;
        --) shift; break;;
    esac
done

if ! [ "$PACKAGE_VERSION" ]; then
  printf "\nPackage parametres are not set. See ./`basename $0` -h\n\n"
  exec $0 -h
  exit 1
fi

apt-get update
apt-get install $PACKAGE_NAME"="$PACKAGE_VERSION

rm -rf /srv/intv/www/config/db.php
rm -rf /srv/intv/www/config/params.php

ln -s ~/config/db.php /srv/intv/www/config/db.php
ln -s ~/config/params.php  /srv/intv/www/config/params.php
cd /srv/intv/www; /srv/intv/www/yii rbac/init
cd /srv/intv/www; /srv/intv/www/yii migrate/up --interactive=0
chown -R intv:intv  /srv/intv/www
service php5-fpm restart
service nginx restart
cat /srv/intv/www/config/crontab | crontab -u intv -
