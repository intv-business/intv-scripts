#!/bin/bash

REPO=https://gitlab.com/intv-business/mq-conf.git
ENC=/opt/mq-enc
CONF=/opt/mq-conf

#/usr/bin/curl -L https://toolbelt.treasuredata.com/sh/install-ubuntu-trusty-td-agent2.sh | sh

cp -r /usr/share/zoneinfo/UTC /etc/localtime

#apt-get update
apt-get -y upgrade
apt-get install -y --no-install-recommends \
nginx-extras git redis-server python3-pip supervisor \
wget lsb-release unzip ca-certificates curl encfs mc \
python3-dev gcc make g++ libc6-dev dh-autoreconf subversion gfortran

pip3 install -U setuptools
pip3 install -U pip

#ksv switch to http for rubygem.org
#td-agent-gem source --remove https://rubygems.org/
#td-agent-gem source --add http://rubygems.org/

#/usr/sbin/td-agent-gem install fluent-plugin-forest

umount ${CONF}
rm -rf $CONF
rm -rf $ENC

mkdir -p $CONF
mkdir -p $ENC

cd $ENC; git clone ${REPO} .

#check if encfs is mounted
MNT="`mount | grep ${CONF} | awk '{print $1}'`"
if [ -z "$MNT" ]; then
    echo "${ENC} is not mounted"
    encfs ${ENC} ${CONF}
    if [ ! $? -eq 0 ]; then
        echo "Fatal error... exit"
        exit 1
    fi
fi
/etc/init.d/nginx stop
/etc/init.d/redis-server stop

update-rc.d -f nginx disable
#update-rc.d -f redis-server disable

/etc/init.d/supervisor stop
cp -a ${CONF}/scripts/delivery/config/supervisord.conf /etc/supervisor/conf.d/
#cp -a ${CONF}/td-agent.conf /etc/td-agent/
#cp -a ${CONF}/fluentd/* /etc/td-agent/plugin/

#ksv change td-agent.conf path
#sed -i 's/\/var\/log\/drip-adserver\//\/var\/delivery\/logs\//' /etc/td-agent/td-agent.conf 

mkdir -p /delivery
mkdir -p /opt/delivery
cp -a ${CONF}/scripts/delivery/* /delivery

ln -s /opt/delivery /var/delivery
mkdir -p /var/delivery/{processed,incoming,logs,aggregates}

adduser --disabled-password --gecos '' celery
chown celery /var/delivery/incoming
chown celery /var/delivery/processed
chown celery /var/delivery/aggregates
chown celery /var/delivery/logs
chmod -R 0775 /var/delivery
chmod -R 0777 /var/delivery/logs

mkdir -p /var/log/celery && chown celery /var/log/celery
mkdir -p /var/run/celery && chown celery /var/run/celery
mkdir -p /var/log/nginx
mkdir -p /var/run/nginx

#double line is necessary
pip3 install -r ${CONF}/scripts/delivery/config/requirements.txt
pip3 install -r ${CONF}/scripts/delivery/config/requirements.txt

echo -n "Please enter redis IP: "
read REDIS_IP

echo -n "Please enter the name of domain used for business layer webserver: "
read DOMAIN

#
sed -i 's/http\:\/\/.*\//https:\/\/'$DOMAIN'\/api\//' /delivery/config.py
sed -i 's/\/delivered/\/data\/delivered/' /delivery/config.py
sed -i 's/REDIS_HOST = "redis"/REDIS_HOST = "127.0.0.1"/' /delivery/config.py
sed -i 's/BUDGET_REDIS_HOST = "127.0.0.1"/BUDGET_REDIS_HOST = "'$REDIS_IP'"/' /delivery/config.py
#sed -i 's/http\:\/\/intv-stg\.ronasit\.com/'$DOMAIN'/' /delivery/config.py

/etc/init.d/redis-server start
/etc/init.d/supervisor start
#/etc/init.d/td-agent start
update-rc.d supervisor defaults
#update-rc.d td-agent defaults

umount ${CONF}
